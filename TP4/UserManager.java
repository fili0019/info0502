import java.util.HashMap;
import java.util.Map;

public class UserManager {
    private Map<String, User> users;

    public UserManager() {
        this.users = new HashMap<>();
    }

    // Create a new user
    public boolean createUser(String userId, String name) {
        if (users.containsKey(userId)) {
            return false; // User already exists
        }
        users.put(userId, new User(userId, name));
        return true;
    }

    // Delete an existing user
    public boolean deleteUser(String userId) {
        if (users.containsKey(userId)) {
            users.remove(userId);
            return true;
        }
        return false;
    }

    // Get a user
    public User getUser(String userId) {
        return users.get(userId);
    }

    // Add score for a user
    public boolean addScore(String userId, String qcmId, int score) {
        User user = users.get(userId);
        if (user != null) {
            user.addScore(qcmId, score);
            return true;
        }
        return false;
    }

    // Retrieve scores for a user
    public Map<String, Integer> getUserScores(String userId) {
        User user = users.get(userId);
        if (user != null) {
            return user.getAllScores();
        }
        return null;
    }
}
