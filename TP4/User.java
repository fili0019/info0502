import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class User implements Serializable {
    private String userId;
    private String name;
    private Map<String, Integer> scores; // QCM ID -> Score

    public User(String userId, String name) {
        this.userId = userId;
        this.name = name;
        this.scores = new HashMap<>();
    }

    // Add score for a QCM
    public void addScore(String qcmId, int score) {
        scores.put(qcmId, score);
    }

    // Get score for a specific QCM
    public Integer getScore(String qcmId) {
        return scores.get(qcmId);
    }

    // Get all scores
    public Map<String, Integer> getAllScores() {
        return scores;
    }

    // Getters
    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }
}
