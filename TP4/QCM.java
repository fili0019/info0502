import java.io.Serializable;
import java.util.List;

public class QCM implements Serializable {
    private String qcmId;
    private String title;
    private List<Question> questions;

    public QCM(String qcmId, String title, List<Question> questions) {
        this.qcmId = qcmId;
        this.title = title;
        this.questions = questions;
    }

    // Getters and setters
    public String getQcmId() {
        return qcmId;
    }

    public String getTitle() {
        return title;
    }

    public List<Question> getQuestions() {
        return questions;
    }
}

class Question implements Serializable {
    private String questionId;
    private String text;
    private List<String> choices;
    private int correctChoice;

    public Question(String questionId, String text, List<String> choices, int correctChoice) {
        this.questionId = questionId;
        this.text = text;
        this.choices = choices;
        this.correctChoice = correctChoice;
    }

    // Getters and setters
    public String getQuestionId() {
        return questionId;
    }

    public String getText() {
        return text;
    }

    public List<String> getChoices() {
        return choices;
    }

    public int getCorrectChoice() {
        return correctChoice;
    }
}
