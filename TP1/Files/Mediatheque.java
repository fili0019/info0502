import java.util.Vector;

public class Mediatheque {
    private String ownerName;
    private Vector<Media> mediaCollection;

    // Constructors
    public Mediatheque() {
        this.ownerName = "";
        this.mediaCollection = new Vector<>();
    }

    public Mediatheque(String ownerName) {
        this.ownerName = ownerName;
        this.mediaCollection = new Vector<>();
    }

    // Method to add media
    public void add(Media media) {
        mediaCollection.add(media);
    }

    @Override
    public String toString() {
        return "Mediatheque[owner=" + ownerName + ", mediaCollection=" + mediaCollection + "]";
    }

    // Main method for testing the class
    public static void main(String[] args) {
        // Create a Mediatheque instance
        Mediatheque mediatheque = new Mediatheque("John Doe");

        // Create some Media objects (assuming you have a Media class)
        Media media1 = new Media("Inception", new StringBuffer("M001"), 5);
        Media media2 = new Media("The Matrix", new StringBuffer("M002"), 4);

        // Add media items to the Mediatheque
        mediatheque.add(media1);
        mediatheque.add(media2);

        // Print out the Mediatheque information
        System.out.println(mediatheque);  // Output the content of the Mediatheque
    }
}
