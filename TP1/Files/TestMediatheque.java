public class TestMediatheque {
    public static void main(String[] args) {
        Mediatheque mediatheque = new Mediatheque("John Doe");
        Livre livre = new Livre("Le Petit Prince", new StringBuffer("LP123"), 5, "Antoine de Saint-Exupéry", "9782070612758");
        Film film = new Film("The Matrix", new StringBuffer("MTRX001"), 5, "Lana Wachowski", 1999);

        mediatheque.add(livre);
        mediatheque.add(film);

        System.out.println(mediatheque);
    }
}
