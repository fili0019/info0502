public class Livre extends Media {
    private String author;
    private String isbn;

    // Constructor
    public Livre(String title, StringBuffer cote, int note, String author, String isbn) {
        super(title, cote, note);
        this.author = author;
        this.isbn = isbn;
    }

    // Getters and Setters
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return "Livre[" + super.toString() + ", author=" + author + ", isbn=" + isbn + "]";
    }
}
