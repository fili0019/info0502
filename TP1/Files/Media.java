public class Media {
    // Données d'instance privées
    private String titre;
    private StringBuffer cote;
    private int note;
    
    // Donnée de classe privée (nom de la médiathèque)
    private static String nomMediatheque = "Mediatheque Inconnue";

    // Constructeur par défaut
    public Media() {
        this.titre = "";
        this.cote = new StringBuffer("");
        this.note = 0;
    }

    // Constructeur par initialisation
    public Media(String titre, StringBuffer cote, int note) {
        this.titre = titre;
        this.cote = cote;
        this.note = note;
    }

    // Constructeur par copie
    public Media(Media media) {
        this.titre = media.titre;
        this.cote = new StringBuffer(media.cote); // Utilisation d'un nouveau StringBuffer
        this.note = media.note;
    }

    // Setters et Getters pour les données d'instance
    public String getTitre() {
        return this.titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public StringBuffer getCote() {
        return new StringBuffer(this.cote);  // Return a copy to avoid external modifications
    }

    public void setCote(StringBuffer cote) {
        this.cote = cote;
    }

    public int getNote() {
        return this.note;
    }

    public void setNote(int note) {
        if(note >= 0 && note <= 10) { // On considère une note entre 0 et 10
            this.note = note;
        } else {
            System.out.println("La note doit être entre 0 et 10.");
        }
    }

    // Méthodes de classe pour gérer le nom de la médiathèque
    public static String getNomMediatheque() {
        return nomMediatheque;
    }

    public static void setNomMediatheque(String nom) {
        nomMediatheque = nom;
    }

    public static void main(String[] args) {
        // Creating Media object using constructor
        Media media1 = new Media("Inception", new StringBuffer("AB123"), 8);
        System.out.println(media1);
    
        // Changing the media title and printing the updated media
        media1.setTitre("The Matrix");
        System.out.println("Updated Media: " + media1);
        
        // Changing cote and printing it
        media1.setCote(new StringBuffer("CD456"));
        System.out.println("Updated Cote: " + media1.getCote());
    
        // Trying to set an invalid note
        media1.setNote(11);  // Should print an error message
    
        // Testing equals method
        Media media2 = new Media(media1);  // Using the copy constructor
        System.out.println("Media1 equals Media2: " + media1.equals(media2));
    }
    

    // Masquage des méthodes clone, equals et toString
    @Override
    public String toString() {
        return "Titre: " + this.titre + ", Cote: " + this.cote.toString() + ", Note: " + this.note;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Media media = (Media) obj;
        return note == media.note &&
                titre.equals(media.titre) &&
                cote.toString().equals(media.cote.toString());
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Media(this);
    }
}
