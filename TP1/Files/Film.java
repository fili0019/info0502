public class Film extends Media {
    private String director;
    private int year;

    // Constructors
    public Film() {
        super();
        this.director = "";
        this.year = 0;
    }

    public Film(String title, StringBuffer cote, int note, String director, int year) {
        super(title, cote, note);
        this.director = director;
        this.year = year;
    }

    public Film(Film other) {
        super(other);
        this.director = other.director;
        this.year = other.year;
    }

    // Getters and Setters
    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Film[" + super.toString() + ", director=" + director + ", year=" + year + "]";
    }
}
