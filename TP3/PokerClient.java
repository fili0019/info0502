package edu.info0502.tp3;

import java.io.*;
import java.net.*;

public class PokerClient {
    private static final String SERVER_ADDRESS = "localhost";
    private static final int SERVER_PORT = 12345;

    public static void main(String[] args) {
        try (Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT)) {
            ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
            BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

            // Handle nickname
            while (true) {
                System.out.println(input.readObject()); // "Enter your nickname:"
                String nickname = console.readLine();
                output.writeObject(nickname);

                String response = (String) input.readObject();
                System.out.println(response);
                if (response.equals("Nickname accepted.")) {
                    break;
                }
            }

            // Messaging loop
            new Thread(() -> {
                try {
                    while (true) {
                        System.out.println(input.readObject());
                    }
                } catch (IOException | ClassNotFoundException e) {
                    System.out.println("Disconnected from server.");
                }
            }).start();

            while (true) {
                String message = console.readLine();
                output.writeObject(message);
                if (message.equalsIgnoreCase("exit")) {
                    System.out.println("Exiting...");
                    break;
                }
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
