package edu.info0502.tp3;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class PokerServer {
    private static final int PORT = 12345;
    private static final Map<String, ObjectOutputStream> clients = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Poker Server is running on port " + PORT);

            while (true) {
                Socket clientSocket = serverSocket.accept();
                new ClientHandler(clientSocket).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class ClientHandler extends Thread {
        private final Socket socket;
        private String nickname;
        private ObjectInputStream input;
        private ObjectOutputStream output;

        public ClientHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                input = new ObjectInputStream(socket.getInputStream());
                output = new ObjectOutputStream(socket.getOutputStream());

                // Nickname handling
                while (true) {
                    output.writeObject("Enter your nickname:");
                    nickname = (String) input.readObject();
                    synchronized (clients) {
                        if (!clients.containsKey(nickname)) {
                            clients.put(nickname, output);
                            output.writeObject("Nickname accepted.");
                            break;
                        } else {
                            output.writeObject("Nickname already in use. Try another one.");
                        }
                    }
                }

                System.out.println(nickname + " has joined the game.");
                broadcast(nickname + " has joined the game.", null);

                // Handle client communication
                while (true) {
                    String message = (String) input.readObject();
                    if (message.equalsIgnoreCase("exit")) {
                        disconnect();
                        break;
                    } else {
                        broadcast(nickname + ": " + message, nickname);
                    }
                }

            } catch (IOException | ClassNotFoundException e) {
                System.out.println(nickname + " disconnected unexpectedly.");
            } finally {
                disconnect();
            }
        }

        private void disconnect() {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (nickname != null) {
                clients.remove(nickname);
                System.out.println(nickname + " has left the game.");
                broadcast(nickname + " has left the game.", null);
            }
        }

        private void broadcast(String message, String sender) {
            clients.forEach((name, out) -> {
                if (sender == null || !name.equals(sender)) {
                    try {
                        out.writeObject(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
