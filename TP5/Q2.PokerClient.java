import org.eclipse.paho.client.mqttv3.*;
import com.google.gson.Gson;

public class PokerClient {
    private static final String BROKER = "tcp://localhost:1883";
    private static final String JOIN_TOPIC = "poker/join";
    private static final String STATE_TOPIC = "poker/state";
    private static final String RESULT_TOPIC = "poker/result";
    private String playerId;

    public PokerClient(String playerId) throws MqttException {
        this.playerId = playerId;
        MqttClient client = new MqttClient(BROKER, MqttClient.generateClientId());
        client.connect();

        // Subscribe to relevant topics
        client.subscribe(STATE_TOPIC, this::handleState);
        client.subscribe("poker/" + playerId + "/cards", this::handleCards);

        // Publish join message
        String joinMessage = new Gson().toJson(Map.of("type", "join", "player_id", playerId));
        client.publish(JOIN_TOPIC, new MqttMessage(joinMessage.getBytes()));
    }

    private void handleState(String topic, MqttMessage message) {
        System.out.println("Game state update: " + new String(message.getPayload()));
    }

    private void handleCards(String topic, MqttMessage message) {
        System.out.println("Your cards: " + new String(message.getPayload()));
    }

    public static void main(String[] args) throws MqttException {
        new PokerClient("player1");
    }
}
