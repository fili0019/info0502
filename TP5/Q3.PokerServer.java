import org.eclipse.paho.client.mqttv3.*;
import com.google.gson.Gson;
import java.util.*;

public class PokerServerMultiTable {
    private static final String BROKER = "tcp://localhost:1883";
    private MqttClient client;
    private Map<String, Table> tables = new HashMap<>();
    private Gson gson = new Gson();

    public PokerServerMultiTable() throws MqttException {
        client = new MqttClient(BROKER, MqttClient.generateClientId());
        client.connect();

        // Subscribe to relevant topics
        client.subscribe("poker/table/create", this::handleCreateTable);
        client.subscribe("poker/table/+/join", this::handleJoinTable);
        client.subscribe("poker/table/+/close", this::handleCloseTable);
    }

    private void handleCreateTable(String topic, MqttMessage message) throws MqttException {
        String payload = new String(message.getPayload());
        Map<String, String> request = gson.fromJson(payload, Map.class);
        String tableId = request.get("table_id");
        String adminId = request.get("player_id");

        if (!tables.containsKey(tableId)) {
            tables.put(tableId, new Table(tableId, adminId));
            System.out.println("Table created: " + tableId + " by admin: " + adminId);

            // Notify the admin of successful creation
            client.publish("poker/table/" + tableId + "/state",
                new MqttMessage(("Table " + tableId + " created").getBytes()));
        }
    }

    private void handleJoinTable(String topic, MqttMessage message) throws MqttException {
        String tableId = topic.split("/")[2];
        String payload = new String(message.getPayload());
        Map<String, String> request = gson.fromJson(payload, Map.class);
        String playerId = request.get("player_id");

        Table table = tables.get(tableId);
        if (table != null && table.addPlayer(playerId)) {
            System.out.println(playerId + " joined table " + tableId);
            client.publish("poker/table/" + tableId + "/state",
                new MqttMessage((playerId + " joined table " + tableId).getBytes()));
        } else {
            client.publish("poker/table/" + tableId + "/state",
                new MqttMessage(("Table full or does not exist: " + tableId).getBytes()));
        }
    }

    private void handleCloseTable(String topic, MqttMessage message) throws MqttException {
        String tableId = topic.split("/")[2];
        Table table = tables.remove(tableId);

        if (table != null) {
            System.out.println("Table closed: " + tableId);
            client.publish("poker/table/" + tableId + "/state",
                new MqttMessage(("Table " + tableId + " closed").getBytes()));
        }
    }

    public static void main(String[] args) throws MqttException {
        new PokerServerMultiTable();
    }
}

class Table {
    private String tableId;
    private String adminId;
    private List<String> players = new ArrayList<>();
    private static final int MAX_PLAYERS = 8;

    public Table(String tableId, String adminId) {
        this.tableId = tableId;
        this.adminId = adminId;
        this.players.add(adminId); // Admin is the first player
    }

    public boolean addPlayer(String playerId) {
        if (players.size() < MAX_PLAYERS) {
            players.add(playerId);
            return true;
        }
        return false;
    }

    public String getAdminId() {
        return adminId;
    }

    public List<String> getPlayers() {
        return players;
    }
}
