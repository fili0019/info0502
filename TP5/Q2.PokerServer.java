import org.eclipse.paho.client.mqttv3.*;
import com.google.gson.Gson;
import java.util.*;

public class PokerServer {
    private static final String BROKER = "tcp://localhost:1883";
    private static final String JOIN_TOPIC = "poker/join";
    private static final String STATE_TOPIC = "poker/state";
    private static final String RESULT_TOPIC = "poker/result";

    private MqttClient client;
    private List<String> players = new ArrayList<>();
    private Deck deck = new Deck();
    private Gson gson = new Gson();

    public PokerServer() throws MqttException {
        client = new MqttClient(BROKER, MqttClient.generateClientId());
        client.connect();
        client.subscribe(JOIN_TOPIC, this::handleJoin);
    }

    private void handleJoin(String topic, MqttMessage message) {
        String payload = new String(message.getPayload());
        Map<String, String> joinMessage = gson.fromJson(payload, Map.class);
        String playerId = joinMessage.get("player_id");

        if (!players.contains(playerId) && players.size() < 8) {
            players.add(playerId);
            System.out.println("Player joined: " + playerId);

            if (players.size() == 2) { // Start game when 2 players join
                startGame();
            }
        }
    }

    private void startGame() {
        try {
            // Shuffle the deck
            deck.shuffle();

            // Publish initial game state
            client.publish(STATE_TOPIC, new MqttMessage("Game starting...".getBytes()));

            // Deal cards and update clients
            for (String player : players) {
                String cards = gson.toJson(deck.dealHand(2));
                client.publish("poker/" + player + "/cards", new MqttMessage(cards.getBytes()));
            }

            // Proceed with the game (e.g., flop, turn, river)
            publishFlop();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void publishFlop() throws MqttException {
        List<Card> flop = deck.dealCards(3);
        client.publish(STATE_TOPIC, new MqttMessage(gson.toJson(flop).getBytes()));
    }

    public static void main(String[] args) throws MqttException {
        new PokerServer();
    }
}
