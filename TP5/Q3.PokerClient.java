import org.eclipse.paho.client.mqttv3.*;
import com.google.gson.Gson;

public class PokerClientMultiTable {
    private static final String BROKER = "tcp://localhost:1883";
    private MqttClient client;
    private Gson gson = new Gson();
    private String playerId;

    public PokerClientMultiTable(String playerId) throws MqttException {
        this.playerId = playerId;
        client = new MqttClient(BROKER, MqttClient.generateClientId());
        client.connect();

        // Subscribe to state updates
        client.subscribe("poker/table/+/state", this::handleStateUpdate);
    }

    public void createTable(String tableId) throws MqttException {
        String payload = gson.toJson(Map.of("type", "create", "table_id", tableId, "player_id", playerId));
        client.publish("poker/table/create", new MqttMessage(payload.getBytes()));
    }

    public void joinTable(String tableId) throws MqttException {
        String payload = gson.toJson(Map.of("type", "join", "player_id", playerId));
        client.publish("poker/table/" + tableId + "/join", new MqttMessage(payload.getBytes()));
    }

    public void closeTable(String tableId) throws MqttException {
        String payload = gson.toJson(Map.of("type", "close", "table_id", tableId));
        client.publish("poker/table/" + tableId + "/close", new MqttMessage(payload.getBytes()));
    }

    private void handleStateUpdate(String topic, MqttMessage message) {
        System.out.println("State update: " + new String(message.getPayload()));
    }

    public static void main(String[] args) throws MqttException {
        PokerClientMultiTable client = new PokerClientMultiTable("player1");

        // Example interactions
        client.createTable("table123");
        client.joinTable("table123");
        client.closeTable("table123");
    }
}
