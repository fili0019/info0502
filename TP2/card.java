package edu.info0502.tp1;

public class Card {
    private String suit; // Hearts, Diamonds, Clubs, Spades
    private String rank; // 2-10, Jack, Queen, King, Ace

    public Card(String rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    @Override
    public String toString() {
        return rank + " of " + suit;
    }
}
