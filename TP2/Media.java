package edu.info0502.tp1;

public class Media {
    private String title;
    private StringBuffer code;
    private int rating;

    // Constructor
    public Media(String title, StringBuffer code, int rating) {
        this.title = title;
        this.code = code;
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Media[title=" + title + ", code=" + code + ", rating=" + rating + "]";
    }
}
