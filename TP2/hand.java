package edu.info0502.tp1;

import java.util.ArrayList;

public class Hand {
    private ArrayList<Card> cards;

    public Hand() {
        this.cards = new ArrayList<>();
    }

    public void addCard(Card card) {
        if (cards.size() < 5) {
            cards.add(card);
        }
    }

    public String evaluateHand() {
        // Simplified evaluation: just listing cards
        return "Hand: " + cards.toString();
    }

    @Override
    public String toString() {
        return cards.toString();
    }
}
