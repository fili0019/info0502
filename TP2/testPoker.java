package edu.info0502.tp1;

public class TestPoker {
    public static void main(String[] args) {
        Deck deck = new Deck();
        deck.shuffle();

        Hand player1 = new Hand();
        Hand player2 = new Hand();
        Hand player3 = new Hand();
        Hand player4 = new Hand();

        for (int i = 0; i < 5; i++) {
            player1.addCard(deck.drawCard());
            player2.addCard(deck.drawCard());
            player3.addCard(deck.drawCard());
            player4.addCard(deck.drawCard());
        }

        System.out.println("Player 1: " + player1.evaluateHand());
        System.out.println("Player 2: " + player2.evaluateHand());
        System.out.println("Player 3: " + player3.evaluateHand());
        System.out.println("Player 4: " + player4.evaluateHand());
    }
}
