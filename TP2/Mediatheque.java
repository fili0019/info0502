package edu.info0502.tp1;

import java.util.Vector;

public class Mediatheque {
    private String ownerName;
    private Vector<Media> mediaCollection;

    // Constructors
    public Mediatheque() {
        this.ownerName = "";
        this.mediaCollection = new Vector<>();
    }

    public Mediatheque(String ownerName) {
        this.ownerName = ownerName;
        this.mediaCollection = new Vector<>();
    }

    // Method to add media
    public void add(Media media) {
        mediaCollection.add(media);
    }

    @Override
    public String toString() {
        return "Mediatheque[owner=" + ownerName + ", mediaCollection=" + mediaCollection + "]";
    }
}
