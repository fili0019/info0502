package edu.info0502.tp1;

import java.util.ArrayList;

public class TexasHoldem {
    private ArrayList<Card> sharedCards;
    private ArrayList<Hand> playersHands;

    public TexasHoldem() {
        this.sharedCards = new ArrayList<>();
        this.playersHands = new ArrayList<>();
    }

    public void addPlayer(Hand hand) {
        playersHands.add(hand);
    }

    public void dealSharedCard(Card card) {
        if (sharedCards.size() < 5) {
            sharedCards.add(card);
        }
    }

    @Override
    public String toString() {
        return "Shared Cards: " + sharedCards + "\nPlayers Hands: " + playersHands;
    }
}
